import React from 'react';
import Wrapper from "../../../hoc/Wrapper";
import './Alert.css';

const Alert = props => (
    <Wrapper>
        <div className="Alert">
            <div class={['alert alert-', props.type].join('')} role="alert">
                <p>{props.children} {props.dismiss ? <button className="Close" onClick={props.dismiss}>X</button> : null}</p>
            </div>
        </div>
    </Wrapper>
);

export default Alert;