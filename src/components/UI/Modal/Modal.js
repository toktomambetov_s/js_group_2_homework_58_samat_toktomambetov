import React from 'react';
import './Modal.css';
import Wrapper from "../../../hoc/Wrapper";

const Modal = props => (
    <Wrapper>
        <div className="Modal"
             style={{
             transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
             opacity : props.show ? '1' : '0'
             }}
        >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{props.title}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick={props.closed}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {props.children}
                        </div>
                    </div>
                </div>
            </div>
    </Wrapper>
);

export default Modal;