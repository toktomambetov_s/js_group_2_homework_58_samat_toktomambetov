import React, { Component } from 'react';
import Alert from "./components/UI/Alert/Alert";
import Wrapper from "./hoc/Wrapper";
import Modal from "./components/UI/Modal/Modal";

class App extends Component {
    state = {
        show: false,
    };

    showModal = () => {
      this.setState({show: true});
    };

    closeModal = () => {
      this.setState({show: false});
    };

    someHandler = () => {
      alert('Close button was clicked!');
    };

  render() {
    return (
        <Wrapper>
            <Modal show={this.state.show}
                   closed={this.closeModal}
                   title="Some kinda modal title"
            >
                <p>This is modal content</p>
            </Modal>
            <button className="ShowModal" onClick={this.showModal}>Show Modal</button>
            <Alert
                type="warning"
                dismiss={this.someHandler}
            >This is a warning type alert</Alert>
            <Alert type="success">This is a success type alert</Alert>
        </Wrapper>
    );
  }
}

export default App;
